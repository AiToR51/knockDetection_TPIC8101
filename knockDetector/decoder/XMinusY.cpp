/*
 * XMinusY.cpp
 *
 *  Created on: 19 feb. 2018
 *      Author: AGR
 */
#include "XMinusY.h"

//TODO
XMinusY::XMinusY(unsigned int x, unsigned int y, unsigned int offsetDegrees) {
	_x = x;
	_y = y;
	_offsetDegrees = offsetDegrees;
	_degreeByPulse = 360 / x;
	pulseCounter = 0;
	previousLastPulseTime = 0;
	lastPulseTime = 0;
	currentPeriod = 0;
	synced = false;
}

XMinusY::~XMinusY() {

}

int XMinusY::getActualDegreesATDC() {
	//si estamos en pulsos, se calcula facil,
	int degrees = -1;
	if (synced) {
		degrees = pulseCounter * _degreeByPulse;
		//long timeInPulse = millis();
		if (pulseCounter == _x - _y - 1) {
			//
		}
	}
	return -1;
}

void XMinusY::interrupt() {

	//check time
	pulseCounter++;
	if (pulseCounter == _x - _y) {
		pulseCounter = 0;
	}
}

void XMinusY::resetInterruptCount() {

}
