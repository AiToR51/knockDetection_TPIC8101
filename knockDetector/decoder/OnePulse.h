/*
 * OnePulse.h
 *
 *  Created on: 19 feb. 2018
 *      Author: AGR
 */

#ifndef DECODER_ONEPULSE_H_
#define DECODER_ONEPULSE_H_
#include <Arduino.h>

class OnePulse {
public:
	~OnePulse();
	OnePulse(unsigned int ignitionEvents4Cycle, unsigned int offsetDegrees);
	int getActualDegreesATDC();
	void interrupt();
private:
	bool isSync();
	unsigned int _ignitionEvents4Cycle;
	unsigned long _minTimeBetweenPulses;
	long _offsetDegrees;
	unsigned long _degreesByPulse;
	unsigned long lastPulseTime;
	unsigned long currentPeriod;
	bool synced;
};


#endif /* DECODER_ONEPULSE_H_ */
