/*
 * xMinusY.h
 *
 *  Created on: 19 feb. 2018
 *      Author: AGR
 */

#ifndef DECODER_XMINUSY_H_
#define DECODER_XMINUSY_H_


class XMinusY {
public:
	~XMinusY();
	XMinusY(unsigned int x, unsigned int y, unsigned int offsetDegrees);
	int getActualDegreesATDC();
	void interrupt();
private:
	unsigned int _x;
	unsigned int _y;
	unsigned int _offsetDegrees;
	unsigned int _degreeByPulse;
	unsigned long previousLastPulseTime;
	unsigned long lastPulseTime;
	unsigned int currentPeriod;
	unsigned int pulseCounter;
	bool synced;
	void resetInterruptCount();
};


#endif /* DECODER_XMINUSY_H_ */
