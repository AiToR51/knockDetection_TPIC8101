/*
 * OnePulse.cpp
 *
 *  Created on: 19 feb. 2018
 *      Author: AGR
 */
#include "OnePulse.h"

//TODO ms2 tacho output - flanco ascendente -10 grados
OnePulse::OnePulse(unsigned int ignitionEvents4Cycle,
		unsigned int offsetDegrees) {
	_offsetDegrees = offsetDegrees;
	_ignitionEvents4Cycle = ignitionEvents4Cycle;
	lastPulseTime = 0;
	currentPeriod = 0;
	synced = false;
	_degreesByPulse = 360 / ignitionEvents4Cycle;
	_minTimeBetweenPulses = 1000000L / (8 * ignitionEvents4Cycle);
	//8 rev por sec.
}

OnePulse::~OnePulse() {

}

int OnePulse::getActualDegreesATDC() {
	int degrees = -1000;
	if (isSync()) {
		noInterrupts();
		unsigned long micros0 = micros() - lastPulseTime;
		interrupts();
		if (micros0 >= currentPeriod) {
			degrees = _offsetDegrees;
		} else {
			degrees = -((_degreesByPulse * micros0) / currentPeriod)
				+ _offsetDegrees;
			if (micros0 > (currentPeriod / 2)) {
				degrees += _degreesByPulse;
			}
		}
//		Serial.print(_degreesByPulse);
//		Serial.print(" ");
//		Serial.print(currentPeriod);
//		Serial.print(" ");
//		Serial.print(micros0);
//		Serial.print(" ");
//		Serial.println(degrees);
	}
	return degrees;
}

bool OnePulse::isSync() {
	noInterrupts();
	if ((micros() - lastPulseTime) > _minTimeBetweenPulses) {
		synced = false;
	} else {
		synced = true;
	}
	interrupts();
	//check time
	return synced;
}

void OnePulse::interrupt() {
	unsigned long currentTime = micros();
	currentPeriod = currentTime - lastPulseTime;
	lastPulseTime = currentTime;
}


