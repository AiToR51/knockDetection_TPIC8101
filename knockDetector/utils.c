#include <Arduino.h>


boolean cycleCheck(unsigned long *lastMillis, unsigned int cycle)
{
 unsigned long currentMillis = millis();
 if(currentMillis - *lastMillis >= cycle)
 {
   *lastMillis = currentMillis;
   return true;
 }
 else
   return false;
}

