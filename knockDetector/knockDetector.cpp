#include <Arduino.h>
#include "TPIC8101.h"
#include "utils.c"
#include "decoder/OnePulse.h"

#include <SPI.h>

#define FREQ TPIC_freq_6_64_kHz
#define INT_TIME 0xCA
#define GAIN 0b10101101
//0b10001110 = 14, ganancia 1.00
//0b10100001 = 33, ganancia 0.4
//0b10101101 = 45, ganancia 0.25
#define MODE MODE_TRIGGER_2_COIL
#define USE_SERIAL false
#define ENGINE_CYLS 4
#define ENGINE_COILS 2
#define DWELL_TIME 4000

#define OUTPUT_VALUE_PIN 5

#define START_WINDOW 5
#define END_WINDOW -20

static const unsigned long MIN_ENGINE_RPM_EVENT_PERIOD_MICROS = 1000000; //1000 rpm = 62500 micros / event
static const unsigned long RPM_ENGINE_PERIOD_CHECKING_MS = 150;
unsigned long lastRPMCheckTime = 0;
unsigned long lastRPMEngineCheckTime = 0;
unsigned long lastEngineRPMEvent = 0;
unsigned long previousEngineRPMEvent = 0;

unsigned int engineRPMPulseCounter = 0;

// set pin 10 as the slave select for the digital pot:
const int slaveSelectPin = 10; //gris
const int intHoldPin = 9; //verde
int currentEngineRPM = 0;

unsigned long scheduledIgnitionEventMicros = 0;

boolean pendingIgnitionEvent = false;

unsigned int lastKnockValue = 0;

OnePulse * pulseManager;

boolean runningWindow = false;

void integrate(int del) {
	digitalWrite(intHoldPin, HIGH);
	delayMicroseconds(del);
	digitalWrite(intHoldPin, LOW);
}

unsigned int analog_knock() {
	return (analogRead(0) + analogRead(0)) / 2;  //amarillo
}

int send_data(int data) {
	digitalWrite(slaveSelectPin, LOW);
	int t = SPI.transfer(data);
	digitalWrite(slaveSelectPin, HIGH);
	return t;
}

unsigned int get_knock() {
	digitalWrite(3, LOW);
	unsigned int valor_picado = 0;
	send_data(0x4c);  //frq 6.64=40KHz 0x40
	//valor_picado = send_data(0x4c); //CPU prescaler 8 Mhz 0x46,16mhz 0x4c
	valor_picado = send_data(0xE0); //channel 1 0xE1

	int tempData = send_data(INT_TIME);
	/*	Serial.print(" ");
	 Serial.print(valor_picado, HEX);
	 Serial.print(" ");
	 Serial.print(tempData, HEX);
	 Serial.print(" ");*/
	if (USE_SERIAL && tempData == FREQ) {
		Serial.println("Error en freq");
	}
	valor_picado |= ((tempData & 0b11000000) << 2);
	send_data(GAIN);

	/*
	 valor_picado = send_data(0b11100000);
	 valor_picado |= ((send_data(0b11100000) & 0b11000000) << 2); //vamos retrasados*/
	digitalWrite(3, HIGH);
	return valor_picado;
}

/**
 * La formula de sacar los uS por grado de cig es= 1000000/rpm * 1/60 * 1/360 = 166666/RPM
 * TODO mover a tabla en lugar de calcular cada vez.
 */
int getMicroDelayByRpmAndWindowDegrees(int rpm, int windowDegrees) {
	return windowDegrees * (166666 / rpm);
}

int getMicroIntTimeByRPM(int rpm) {
	int analyzeTime = 0;
	if (rpm <= 250) {
		analyzeTime = getMicroDelayByRpmAndWindowDegrees(250, 40);
	} else if (rpm <= 1000) {
		analyzeTime = getMicroDelayByRpmAndWindowDegrees(rpm, 40);
	} else if (rpm <= 3000) {
		analyzeTime = getMicroDelayByRpmAndWindowDegrees(rpm, 45);
	} else {
		analyzeTime = getMicroDelayByRpmAndWindowDegrees(rpm, 48);
	}
	return DWELL_TIME + analyzeTime;
}

void calcRPM(unsigned long lastRPMEvent, unsigned long previousRPMEvent,
		int * currentRPM, unsigned long minPeriod) {
	noInterrupts();
	if ((micros() - lastRPMEvent) > minPeriod || previousRPMEvent == 0) {
		*currentRPM = 0;
	} else {
		*currentRPM = 60000000 / (lastRPMEvent - previousRPMEvent);
	}
	if ( MODE == MODE_TRIGGER_2_COIL) {
		*currentRPM /= 2;
	}
	interrupts();
}

void ignitionEvent() {
	engineRPMPulseCounter++;
	previousEngineRPMEvent = lastEngineRPMEvent;
	lastEngineRPMEvent = micros(); //overflow cada 70mins aprox.
	pendingIgnitionEvent = true;
}

void interruptOnePulse() {
	pulseManager->interrupt();
}

void setup() {
	if (USE_SERIAL) {
		Serial.begin(115200);
	}
	// set the slaveSelectPin as an output:
	//pinMode (slaveSelectPin, OUTPUT);
	pinMode(intHoldPin, OUTPUT);
	//pinMode(3, OUTPUT);
	// pinMode (52, OUTPUT);
	//pinMode (12, INPUT); //MISO, Salida chip - entrada arduino
	pinMode(slaveSelectPin, OUTPUT);
	digitalWrite(slaveSelectPin, HIGH);
	pinMode(OUTPUT_VALUE_PIN, OUTPUT);
	digitalWrite(OUTPUT_VALUE_PIN, LOW);

	pulseManager = new OnePulse(2, -10); //test
	attachInterrupt(1, interruptOnePulse, RISING);

	//digitalWrite(3, HIGH);
	delay(3000);

	// initialize SPI:
	if (USE_SERIAL) {
		Serial.println("CONFIGURANDO chip - spi");
	}
	//digitalWrite(52, HIGH);
	delay(2000);

	SPI.begin();
	SPI.setClockDivider(SPI_CLOCK_DIV32);

	SPI.setDataMode(SPI_MODE1);
	SPI.setBitOrder(MSBFIRST);

	//digitalWrite(slaveSelectPin,LOW);
	delay(1);

	digitalWrite(intHoldPin, LOW);
	//Serial.println(SPI.transfer(0b01000110),BIN);delayMicroseconds(24); //CPU prescaler 8 Mhz
//	while (true)
//		for (int i = 0; i <= 8000; i += 100) {
//			Serial.print("RPM ");
//			Serial.print(i);
//			Serial.print(" ");
//			Serial.println(getMicroIntTimeByRPM(i));
//		}
//	while (!true) {
//
//		Serial.println(send_data(0x71), HEX);
//		Serial.println(send_data(0x2A), HEX);
//		Serial.println(send_data(0x2A), HEX);
//
//		delay(5000);
//	}
	// Serial.println(send_data(0b01110001),HEX);
	Serial.println(send_data(0x71), HEX); //advanced mode 0x71

	Serial.println(send_data(0x4c), HEX); //CPU prescaler 8 Mhz 0x46,16mhz 0x4c
	Serial.println(send_data(0xE0), HEX); //channel 1 0xE1

	Serial.println(send_data(FREQ), HEX); //frq 6.64=40KHz 0x40

	//Serial.println(send_data(0b00101000),HEX);  //frq 6.64=40KHz 0x40

	Serial.println(send_data(GAIN), HEX); // gain
	//Serial.println(send_data(0b10100000),BIN);
	Serial.println(send_data(INT_TIME), HEX);  // integrator 55us 0xC3
	Serial.println(send_data(0x71), HEX); //advanced mode 0x71
	delay(1500);
	//registramos interrupciones
//	if (MODE != MODE_CONTINUOUS) {
//		attachInterrupt(0, ignitionEvent, FALLING);
//	} else {
//		currentEngineRPM = 4000;
//	}
//
//	if ( MODE == MODE_TRIGGER_2_COIL || MODE == MODE_TRIGGER_3_COIL
//			|| MODE == MODE_TRIGGER_4_COIL) {
//		attachInterrupt(1, ignitionEvent, FALLING);
//		//TODO registrar outros eventos
//	}
	delay(1500);
}

bool hasToEvaluate() {
	if (MODE == MODE_CONTINUOUS) {
		return true;
	} else {
		//miramos si hay algo agendado para este instante.
		return pendingIgnitionEvent;
	}
}

bool hasToRunWindow() {
	int degrees = pulseManager->getActualDegreesATDC();
	return (START_WINDOW >= degrees && END_WINDOW <= degrees);
}

void loop() {
	// Serial.print("lendo datos spi: ");
	// get_knock();

//	if (cycleCheck(&lastRPMEngineCheckTime, RPM_ENGINE_PERIOD_CHECKING_MS)) {
//		calcRPM(lastEngineRPMEvent, previousEngineRPMEvent, &currentEngineRPM,
//				MIN_ENGINE_RPM_EVENT_PERIOD_MICROS);
//		//currentEngineRPM = currentEngineRPM;
//	}
	//Serial.println(pulseManager->getActualDegreesATDC());
	if (hasToRunWindow()) {
		if (!runningWindow) {
			digitalWrite(intHoldPin, HIGH);
			runningWindow = true;
		}
//		int integrateTime = getMicroIntTimeByRPM(currentEngineRPM);
		//digitalWrite(13, HIGH);
//		integrate(integrateTime);
		//digitalWrite(13, LOW);

	} else {
		if (runningWindow) {
			digitalWrite(intHoldPin, LOW);
			runningWindow = false;
			unsigned int analogKnock = analog_knock();
			unsigned int value2Use = max(analogKnock, lastKnockValue);
			lastKnockValue = analogKnock;
			if (USE_SERIAL) {
				Serial.print("RPM: ");
				Serial.print(currentEngineRPM);
				Serial.print(" Analog: ");
				Serial.print(analogKnock);
				Serial.print(" Digital: ");
				Serial.println(get_knock());
			}
			analogWrite(OUTPUT_VALUE_PIN, value2Use / 4);

			pendingIgnitionEvent = false;
		}

	}
	//delay(1);
}
